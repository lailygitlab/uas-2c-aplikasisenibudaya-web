<?php
    $DB_NAME = "budaya";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $mode = $_POST['mode'];
        $respon = array(); $respon['kode'] = '000';
        switch($mode){
            case "insert":
            $id_tari = $_POST['id_tari'];
            $nama_tarian = $_POST['nama_tarian'];
            $id_daerah = $_POST['id_daerah'];
 	        $sejarah_singkat = $_POST['sejarah_singkat'];
            $imstr = $_POST['image'];
            $file = $_POST['file'];
            $path = "images/";

            $sql = "select id_daerah from daerah where daerah = '$id_daerah'";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result)>0){
                $data = mysqli_fetch_assoc($result);
                $id_daerah = $data['id_daerah'];

                $sql = "insert into tarian(id_tari, nama_tarian, id_daerah, sejarah_singkat, foto) values(
                    '$id_tari','$nama_tarian','$id_daerah','$sejarah_singkat','$file'
                )";
                $result = mysqli_query($conn,$sql);
                if($result){
                    if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                        $sql = "delete from tarian where id_tari = '$id_tari'";
                        mysqli_query($conn,$sql);
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }else{
                        echo json_encode($respon); exit();
                    }
                }else{
                    $respon['kode'] = "111";
                    echo json_encode($respon); exit();
                }
            }
            break;
            case "update":
                $id_tari = $_POST['id_tari'];
                $nama_tarian = $_POST['nama_tarian'];
                $id_daerah = $_POST['id_daerah'];
 	            $sejarah_singkat = $_POST['sejarah_singkat'];
                $imstr = $_POST['image'];
                $file = $_POST['file'];
                $path = "images/";

                    $sql = "select id_daerah from daerah where daerah = '$id_daerah'";
                    $result = mysqli_query($conn,$sql);
                    if(mysqli_num_rows($result)>0){
                        $data = mysqli_fetch_assoc($result);
                        $id_daerah = $data['id_daerah'];
                        $sql = "";
                        if($imstr == ""){
                            $sql = "update tarian set nama_tarian = '$nama_tarian', id_daerah = '$id_daerah',  
                            sejarah_singkat = '$sejarah_singkat' where id_tari = '$id_tari'";
                            $result = mysqli_query($conn,$sql);
                            if($result){
                                echo json_encode($respon); exit();
                            }else{
                                $respon['kode'] = "111";
                                echo json_encode($respon); exit();
                            }
                        }else{
                           if(file_put_contents($path.$file, base64_decode($imstr)) == false){
                                $respon['kode'] = "111";
                                echo json_encode($respon); exit();
                            }else{
                                $sql = "update tarian set nama_tarian = '$nama_tarian', id_daerah = '$id_daerah',  
                            sejarah_singkat = '$sejarah_singkat' where id_tari = '$id_tari'";
                                $result = mysqli_query($conn,$sql);
                                if($result){
                                    echo json_encode($respon); exit();
                                }else{
                                    $respon['kode'] = "111";
                                    echo json_encode($respon); exit();
                                }  
                            }
                        }
                    }
                    break;
                    case "delete":
                    $id_daerah = $_POST['id_daerah'];
                    $sql = "select foto from tarian where id_tari = '$id_tari'";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        if(mysqli_num_rows($result)>0){
                            $data = mysqli_fetch_assoc($result);
                            $foto = $data['foto'];
                            $path = "images/";
                            unlink($path.$foto);
                        }
                        $sql = "delete from tarian where id_tari = '$id_tari'";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            echo json_encode($respon); exit();
                        }else{
                            $respon['kode'] = '111';
                            echo json_encode($respon); exit();
                        }
                    }
                break;
            }
    }
?>